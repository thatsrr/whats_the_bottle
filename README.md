# What's the bottle

Firstly install all dependencies:
```bash
pip install -r requirements.txt
```
To run locally:
```bash
python main.py
```
To run tests:
```bash
cd test
python unit_test.py
```