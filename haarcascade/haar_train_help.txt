# Run in haarcascade folder

# To create samples:
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_createsamples.exe -img hc_0\star.jpg -num 1000 -bg hc_0\negatives.dat -vec samples.vec -maxxangle 0.6 -maxyangle 0 -maxzangle 0.3 -maxidev 100 -bgcolor 0 -bgthresh 0 -w 20 -h 20

C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_createsamples.exe -img hc_0\star.jpg -num 1000 -bg hc_0\negatives.dat -vec samples.vec -maxxangle 0.6 -maxyangle 0 -maxzangle 0.3 -maxidev 100 -bgcolor 0 -bgthresh 0 -w 100 -h 100

# To create classifier:
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data hc_0 -vec hc_0\samples.vec -bg hc_0\negatives.dat -nstages 20 -nsplits 2 -minhitrate 0.999 -maxfalsealarm 0.5 -npos 1 -nneg 1000 -w 20 -h 20 -nonsym -mem 512 -mode ALL
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data hc_0 -vec hc_0\samples.vec -bg hc_0\negatives.dat -nstages 20 -maxfalsealarm 0.5 -npos 1 -nneg 1000 -w 20 -h 20

C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data hc_0 -vec hc_0\samples.vec -bg hc_0\negatives.dat -nstages 20 -npos 1 -nneg 4000 -w 100 -h 100

C:\Users\i331722\AppData\Local\OpenCV\opencv_old\opencv\build\x64\vc14\bin\opencv_haartraining.exe -data hc_0 -vec hc_0\samples.vec -bg hc_0\negatives.dat -nstages 10 -npos 1 -nneg 1000 -w 100 -h 100 -nsplits 2

# To create samples:
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_createsamples.exe -img hc_1\star.jpg -num 1000 -bg hc_1\negatives.dat -vec samples.vec -maxxangle 0.6 -maxyangle 0 -maxzangle 0.3 -maxidev 100 -bgcolor 0 -bgthresh 0 -w 20 -h 20

# To create classifier:
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data hc_1 -vec hc_1\samples.vec -bg hc_1\negatives.dat -nstages 20 -nsplits 2 -minhitrate 0.999 -maxfalsealarm 0.5 -npos 1 -nneg 1000 -w 20 -h 20 -nonsym -mem 512 -mode ALL
C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data hc_1 -vec hc_1\samples.vec -bg hc_1\negatives.dat -nstages 20 -maxfalsealarm 0.5 -npos 1 -nneg 1000 -w 20 -h 20