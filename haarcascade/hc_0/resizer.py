import os
import cv2

for num, image in enumerate(os.listdir('neg/')):
    image_url = os.getcwd() + '\\neg\\' + image
    image_resized = cv2.imread(image_url, 0)
    try:
        image_resized = cv2.resize(image_resized, (100, 100))
        cv2.imwrite('./resized/{}.jpg'.format(num), image_resized)
    except:
        print('Oops')
        pass
    
