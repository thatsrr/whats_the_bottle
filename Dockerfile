# Installing virtual machine for running python
#FROM python:3.4-alpine
FROM python:3.5

COPY . /app/

# Installing a virtual environment
# for determining a limits and workspaces
# with different libraries
RUN pip install virtualenv
RUN virtualenv merch_matcher

# Adding work directory
#ADD . /merch_matcher

# Switching to workdirectory
WORKDIR /merch_matcher

# Adding folder to python path
RUN export PYTHONPATH="$PYTHONPATH:/merch_matcher"
ENV PYTHONPATH "${PYTHONPATH}:/merch_matcher"

# Installing all libraries for script running
RUN pip install -r requirements.txt

# Running python script
CMD ['python', 'main.py']