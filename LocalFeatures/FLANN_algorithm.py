import cv2
import numpy as np
from skimage.measure import compare_ssim
from matplotlib import pyplot as plt

logo = cv2.imread('./logo0.png', 0)
# logo = cv2.imread('/image0.jpg', 0)
#imag = cv2.imread('./image0.jpg', 0)
# imag = cv2.imread('image1.jpg', 0)
# imag = cv2.imread('./image2.png', 0)
# imag = cv2.imread('./image2.jpg', 0)
# imag = cv2.imread('./image3.jpg', 0)

imag = cv2.imread('./2.jpg', 0)

#logo = cv2.Canny(logo, 50, 200)
#imag = cv2.Canny(imag, 50, 200)

# Init star detector
orb = cv2.ORB_create()

# Detect key points
kpL = orb.detect(logo, None)
kpI = orb.detect(imag, None)

# Compute descriptors
kpL, dsL = orb.compute(logo, kpL)
kpI, dsI = orb.compute(imag, kpI)

logo_kp = cv2.drawKeypoints(logo, kpL, (0,0,255), flags=0)
imag_kp = cv2.drawKeypoints(imag, kpI, (0,0,255), flags=0)
cv2.imshow('Key points logo', logo_kp)
cv2.imshow('Key points image', imag_kp)
cv2.waitKey()
cv2.destroyAllWindows()


'''
_____________________________
          Brute Force
_____________________________
'''

# create Brute Force Matcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

# Match descriptors.
matches = bf.match(dsL,dsI)

# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

print('Number of descriptors: {}\nNumber of descriptors: {}\nNumber of matches: {}'.format(len(dsL),len(dsI),len(matches)))
# Draw first 20 matches.
img3 = cv2.drawMatches(logo,kpL,imag,kpI,matches[:20], flags=2, outImg=None)

plt.imshow(img3),plt.show()

'''
_____________________________
            SIFT
_____________________________
'''

# Initiate SIFT detector
sift = cv2.xfeatures2d.SIFT_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(logo,None)
kp2, des2 = sift.detectAndCompute(imag,None)

# BFMatcher with default params
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)

# Apply ratio test
good = []
for m,n in matches:
    if m.distance < 0.75*n.distance:
        good.append([m])

print('Number of descriptors: {}\nNumber of descriptors: {}\nNumber of matches: {}'.format(len(dsL),len(dsI),len(matches)))

# cv2.drawMatchesKnn expects list of lists as matches.
img3 = cv2.drawMatchesKnn(logo,kp1,imag,kp2,good,flags=2,outImg=None)

plt.imshow(img3),plt.show()



'''
_____________________________
            FLANN
_____________________________
'''

# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)   # or pass empty dictionary

flann = cv2.FlannBasedMatcher(index_params,search_params)

matches = flann.knnMatch(des1,des2,k=2)

# Need to draw only good matches, so create a mask
matchesMask = [[0,0] for i in range(len(matches))]

# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.7*n.distance:
        matchesMask[i]=[1,0]

draw_params = dict(matchColor = (0,255,0),
                   singlePointColor = (255,0,0),
                   matchesMask = matchesMask,
                   flags = 0)

print('Number of descriptors: {}\nNumber of descriptors: {}\nNumber of matches: {}'.format(len(dsL),len(dsI),len(matches)))

img3 = cv2.drawMatchesKnn(logo,kp1,imag,kp2,matches,None,**draw_params)

plt.imshow(img3,),plt.show()


'''
_____________________________
            SSIM
_____________________________
'''

img0 = cv2.cvtColor(logo, cv2.COLOR_BAYER_BG2GRAY)
img1 = cv2.cvtColor(imag, cv2.COLOR_BAYER_BG2GRAY)
if img0.shape[0] != img1.shape[0] and img0.shape[0] != img1.shape[0]:
    cv2.resize(img1, (img0.shape[1], img1.shape[0]))
(score, diff) = compare_ssim(img0, img1, full = True)
diff = (diff*255).astype("uint8")
print("SSIM: {}\nDifference: {}".format(score, diff))

